# Cal Poly Gateway Site Documentation

This is a Git repository containing all of the documentation for editors and publishers on how to use the Drupal backend of the [Gateway](www.calpoly.edu) and [Events](https://events.calpoly.edu) sites.

The docs in this repository are written in Markdown and hosted by [Read the Docs](https://gateway-site-documentation.readthedocs.io/en/latest/). When commits are pushed to this repository, Read the Docs compiles the Markdown into HTML and automatically updates the documentation.

[View the latest documentation here](https://gateway-site-documentation.readthedocs.io/en/latest/).

## Additional Links/Resources

- [Read the Docs Home](https://www.readthedocs.org)
- [Information on Markdown](https://www.markdownguide.org/getting-started/)
- [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)
- [Markdown Table Generator](https://www.tablesgenerator.com/markdown_tables)
- [Presentation explaining Read the Docs](https://www.youtube.com/watch?v=U6ueKExLzSY)

## Maintainers

This documentation is currently being maintained by [Richard Robinson](mailto:rrobin11@calpoly.edu) and [Shari Sullivan](mailto:oconnors@calpoly.edu) at Cal Poly University Communications and Marketing.
