# Misc.

## Editing / Adding Taxonomy Terms

Taxonomy Terms are used to connect, relate, and classify content on the website.

1. From the top Admin menu: Structure > Taxonomy > [category you wish to edit] > Edit.

![Misc conten example ](img/misc-content-example.png)

<ol start="2">
<li>Click ‘List’ tab.</li>
<li>Click ‘Add term’ button to add new taxonomy term.</li>
<li>Click the arrow to the right of the ‘Edit’ button to open dropdown and select Delete to delete taxonomy term.</li>
<li>Click ‘Save’ to save your work.</li>
</ol>

\*The only taxonomy category that is currently using the Edit function is Historical; see  [Historical Fact](en/latest/content-types/#historical-fact) > Editing the Timeline Table

---

## Editing Custom Blocks

Blocks are boxes of content that are displayed on specified pages. They are currently being used in the Major Transfer Information content type (Transfer Course Criteria pages) and at the bottom of the newsletter. Edits made to the custom blocks will appear on all pages that have that block on it, e.g. edits made to the Major Transfer Top Content block will appear on all ~80 Transfer Course Criteria pages.


### Major Transfer Top Content
![Major Transfer Block example](img/misc-major-transfer-block.png)


### Newsletter Bottom Block Content

![Newsleter Bottom Block Content example](img/misc-newsletter-bottom-block.png)


1. From the top Admin menu: Structure > Block Layout > Custom Block Library.
2. Click ‘Edit’ button of the block you wish to edit (Major Transfer Top Content or Newsletter Bottom Block Content).
3. Make your edits in the WYSIWYG screen; the Newsletter Bottom Block Content also has an image field.
4. Click ‘Save’ to save your work.
