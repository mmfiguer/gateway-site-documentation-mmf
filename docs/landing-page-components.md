# Landing Page Components  

![Screenshot of a dropdown menu with all of the landing page components](img/landing-page-components/landing-page-components-drop-down.png)

Landing Page Components are a collection of different small design layouts you can insert in your pages. The layouts (or components) may consist of an image next to a paragraph, a grouping of boxed content (cards), or a formatted list of links. They typically use WYSIWYG fields. Each are configured differently and sometimes with different style capabilities. Below we document the elements unique to specific components.

---

## Button List

![Screenshot of the button list component](img/landing-page-components/button-list.png)

1. Enter (optional) intro content in the WYSIWYG field.
2. Enter URLs and Link Text for your button links (all text must be linked).

You can be reorder the links in two ways. You can set row "weight" in the column to the right of your content (the lower the weight, the higher it goes on the list); or (with row weights hidden) you can use the intersecting-arrows icon on the left to drag and drop the items into the desired order.

---

## Content List Award

 ![Screenshot of the content list award component](img/landing-page-components/content-list-award.png)

1. Select Award Type from dropdown.
2. Enter Title to appear above awards.

The Drupal system automatically adds a ‘View More’ button at the bottom of the component (clicking it takes the use to the [Rankings and Awards](https://www.calpoly.edu/awards) page.)

---

## Content List College

![Screenshot of the content list college component](img/landing-page-components/content-list-college.png)

1. You may enter optional content (header and description) to appear above list of colleges.

Note: Upon rollover, the following happens:

* A screen is applied over the image to darken the image
* Text moves up and
* A ‘View College’ button appears. See the Orfalea College of Business box (in the screenshot above) for an example.

---

## Content List Events

![Screenshot of the content list events component](img/landing-page-components/content-list-events.png)

1. Select Event Type from dropdown.

Notes:

- The Drupal system automatically adds the 'Cal Poly Events' title.
- The Drupal system automatically adds a  ‘View All Upcoming Events’ button; clicking it takes the user to the [Events site](https://events.calpoly.edu).

---

## Content List Historical Facts

![Screenshot of the Content List Historical Facts component](img/landing-page-components/content-list-historical-facts.png)
To insert an already created Historical Facts component into a landing page and customize its title

1. Select Historical Fact category from dropdown.
2. Enter Title appear above Historical Facts timeline.

---

## Content List News

![Screenshot of the Content List News component](img/landing-page-components/content-list-news.png)
These instructions tell you how to customize an inserted news list into a landing page

1. Select News Category from dropdown.

Note: The Drupal system automatically adds a ‘View All Cal Poly News’ button; clicking it takes the user to the [News](https://www.calpoly.edu/news) home page.

---

## Curated Stories

![Screenshot of the Curated Stories component](img/landing-page-components/curated-stories.png)

1. Enter optional title and intro text.
2. Add an image for first card/story (see [Adding Media to the Site](/en/latest/adding-media/)). This image should be the same dimensions as the image used for the third story.
3. Enter text in the Content field if you are entering simple text, with no styling or links; use the Content HTML field if you need to use features in the WYSIWYG editor. The text should run to the same number of lines as that in the third story.
4. Enter optional URL and link text. (Note: While the links are optional, this component was intended to link to other content.)
5. Repeat for two other cards.

Note: The first and third cards must have same-size images and same number-of-lines of copy. Otherwise, the component will look off-balance and amateur.

---

## Factoid List

![Screenshot of the Factoid List component](img/landing-page-components/factoids.png)

The Factoid list is made up of individual Factoid blocks, centered on the page. Depending on the device, the blocks will wrap to the next line after two or four blocks are displayed.

![Screenshot of the Factoid List component with examples of different fields being used](img/landing-page-components/factoids-with-fields.png)

​​The screenshot above looks bad and shows how the different fields display and that care is required when choosing text sizing and placement. In this example, we  demonstrate how different sizes display (the bracketed text sizes appear after the main fields). If you choose a text option that is too large for the copy, you lose the effectiveness of the message and display.

1. Add image (see [Adding Media to the Site](/en/latest/adding-media/), if needed). These images will display very small and within a circle. See last block in the list above for an example.
2. Enter Intro content (optional).
3. Enter Value text.
4. Select Value text size from the dropdown.
5. Enter Unit text (optional).
6. Enter Context text (optional).
7. Select Factoid Background (background options are called out above).

Note: we recommend backgrounds are used in the following order:

1. CA
1. Topo
1. Default green
1. Secondary
1. Topo 2
1. Tertiary
1. Shield
1. Default green

If you have more than eight boxes, the order would repeat beginning with the ninth box being designated as CA.

---

## Featured Historical Fact

![Screenshot of the Featured Historical Fact component](img/landing-page-components/featured-historical-fact.png)

1. Add Image (see [Adding Media to the Site](/en/latest/adding-media/)).
2. Enter Date (optional).
3. Enter Title.
4. Enter URL and link text (this will become a button).

---

## Featured Quote

![Screenshot of the Featured Quote component](img/landing-page-components/featured-quote.png)

1. Add image (see [Adding Media to the Site](/en/latest/adding-media/)).
2. Enter Quote text.
3. Enter Quoter text.
4. Enter Quote label. This will most often be the major of the person quoted and the year of graduation.
5. Enter Quoter description.

---

## Full-Width Video or Image w/Content

![Screenshot of the Full-Width Video or Image w/Content component](img/landing-page-components/full-width-image-with-video.png)

1. Add image (see [Adding Media to the Site](/en/latest/adding-media/)).
2. Enter content that appears to the right on the image. This should be short;  no more than two lines when viewed in the Abolition typeface.
3. Enter caption that appears below the image (optional). Note that in most cases you will not need/use the caption: this an option in only because the text to the right of the image must be brief.

---

## Generic Content Grid

The Generic Content Grid is a very versatile landing page component, and can look quite different depending on the options you select. Below are a few examples.

![Screenshot of the Generic Content Grid component](img/landing-page-components/generic-content-grid-left-aligned-no-image.png)

* Simple headline in the optional intro area with no intro copy
* Left-aligned content in the cards
* No images

![Screenshot of the Generic Content Grid component with centered text, round images, and buttons](img/landing-page-components/generic-content-grid-center-aligned-with-image.png)

* Headline and intro copy in optional intro area
* Center-aligned content in the cards
* Images w/rounded corners (icons, in this example: note that photos w/people in them do not work well for round images)

![Screenshot of the Generic Content Grid component with images and left-aligned text](img/landing-page-components/generic-content-grid-left-aligned-with-image-copy.png)

* Headline w/brief intro copy in optional intro area
* Left-aligned content in the cards
* Images

Note: These display in a grid of three across, and you can have as many rows as you’d like. If there are less than three cards in a row the cards will center-align.

1. In the Content Align dropdown, select either ‘Left ‘or ‘Center’.
2. Enter Intro content.
3. Add image for first card (see [Adding Media to the Site](/en/latest/adding-media/)).
4. Check ‘Image with round corner’ if you want your image cropped in a circle. Note that this can only be done with center-aligned content.
5. Enter content for card.
6. Click ‘Add Generic Card’ button to add another card.

Recommendations

* Ensure your images are the same dimensions (example: 16:9)
* Make your images smaller (example: 800 px width) for better page performance

---

## Horizontal Tabs

![Screenshot of the Horizontal Tabs component](img/landing-page-components/horizontal-tabs.png)

1. For each tab Content

  * Enter tab Title.
  * Enter content to be displayed in tab in the Content field
2. To add a tab

  *  Click ‘Add Horizontal Tab’ to add another tab
  * Do not use more than four tabs

---

## Image Banner

![Screenshot of the Image Banner component](img/landing-page-components/image-banner.png)

An Image Banner is a full-width, parallax display used to both reinforce the page content as well as to break up content on a long scrolling page; especially helpful when two components won't look good stacked on the page (as in the above example).

1. Add image (see [Adding Media to the Site](/en/latest/adding-media/)).

---

## Images Gallery

![Screenshot of the Images Gallery component](img/landing-page-components/images-gallery.png)

While the Images Gallery component uses fields you have seen in other templates and components — a WYSIWYG intro area and a field to upload images - it is different because it requires images uploaded as 'Image with Position'.

1. Enter Intro content.

![Screenshot of the the form to create or edit the Images Gallery component](img/landing-page-components/images-gallery-image-with-position-edit.png)


<ol start="2">
<li>Add Image, either by clicking ‘Media add page’ link or selecting it by using the type-ahead field.</li>
<li> To upload an image
<ol><li> Enter image Name. This is what will appear in the list of /admin/content/media.</li>
<li> Select Image Orientation from the dropdown,</li>
<li>Enter Alternative Text. Note that this will also appear in the Images Gallery as the photo caption that the user sees when hovering over the image.</li>
<li> Click 'Save' to save your work. This will take you back to the edit screen on the page you are adding the Images Gallery to.</ol>
<li> Click 'Add another item' button to add more images. </li>
</ol>


---

## Major Finder

![Screenshot of the Major Finder component](img/landing-page-components/major-finder.png)

To add the Major Finder to a page, simply select it from the dropdown. There is no content to enter and there are no settings to select.

---

## Simple Content

![Screenshot of the Simple Content component](img/landing-page-components/simple-content.png)

The Simple Content component is simply a WYSIWYG field, with the option to center- or left-align your content. Above are example of each.

1. Enter content in WYSIWYG.
2. From the Content Align dropdown, select Left or Centered.

---

## Split Content

![Screenshot of the Split Content component](img/landing-page-components/split-content.png)

1. Enter content in Content Left field.
2. Enter content in Content Right field.

Note: for the links, all content in each line must be linked. You can have no unlinked content.

---

## Step-by-Step

![Screenshot of the Step-by-Step component](img/landing-page-components/step-by-step.png)

This content type is basically the same as the [Generic Content Grid](#generic-content-grid), with the addition of the shield that contains the number. Content is entered in cards that display three per row; if there are less than three cards in a row they will be centered.

1. Enter Intro content.
2. Add image (see [Adding Media to the Site](/en/latest/adding-media/)).
3. Enter Eyebrow text (optional).
4. Enter Title.
5. Enter card content. Use the Content field if you are entering only text, or the Content HTML field if you want to use add styling and format available via the WYSIWYG editor.
6. Add link URL and Link text.
7. Click ‘Add Card’ button to add another card.

Note: Images, like all fields that do not have a red asterisk next to the label, are optional.

---

## Two Columns with Image List

There are two variations of the Two Columns with Image List, the most widely used component on landing pages.

### Row Style: Content Style

![Screenshot of the Two Columns with Image List component in the "content" style](img/landing-page-components/two-columns-with-image-content.png)

In the above example:

Select/choose

* ‘Content Style’ has been selected from the Row Style dropdown
* Image Position is Left.

Optional: Stack Two Column with images

You can also add another Two Column with Image below this and set the Image Position to Right to create an alternating grid layout.

### Row Style: Full-Width

![Screenshot of the Two Columns with Image List component in the "full-width" style](img/landing-page-components/two-columns-with-image-full-width.png)

With Row Style: ‘Full-Width’ selected, the text is displayed over a green background and both the text block and the image go the full-width of the page. You cannot add stack Two Column with Image components when ‘Full-Width’ is selected, however there is an optional Intro content area.

1. Enter Intro content in the Content Top WYSIWYG field.
2. Select:

* Row Style: ‘Content Style’ for black text over white background
* ‘Full-Width’ for white text over green background and full-width component.
3. Enter copy that will appear next to the image in the Content field.
4. Add image (see [Adding Media to the Site](/en/latest/adding-media/)).
5. Set image to appear on the left or the right using the Image Position dropdown.
6. To add another Two Columns with Image List row, click ‘Add OLD—Two Columns with Image Row’ button. Reminder: you can only stack this component if you have selected the Content Style row style.
