# Adding Media to the Site

​​When you choose to create a web page (via “Add content”), you can add media at the same time that you add your copy if you are using a Simple Content component.

​​However, some pages or templates, like landing pages, use more complex components that require media to be uploaded before you create your page. This includes featured images, hero images, and component images.

​​You add these images using the “Add media” upload tool. Once the images are successfully uploaded, they become accessible to you with a type-ahead field.

## Images

![Add Media screenshot](img/adding-media/adding-media-edit-1.png)

1) From the top Admin menu, select Content > Add Media > Image.

![Editing Add Media screenshot](img/adding-media/adding-media-edit-2.png)

<ol start="2">
<li>​​Enter a name for the file. This provides a way to identify the file after it’s been uploaded. You can enter the actual file name, but we recommend renaming it to describe its site section, subject, or even size. Try to name it uniquely.</li>
<li>Click ‘Choose File’ to upload image. (Until you upload the image this field will appear as it does in the upper right corner of the screenshot; the thumbnail image and the Alternative text field are displayed after the image is uploaded.)</li>
<li>Enter Alternative text.</li>
<li>Click ‘Save’ to complete the upload.</li>
</ol>

\* Note: These are instructions for “Image.” If you are uploading images meant for a landing-page image gallery, choose “Image with Position” instead. This allows you to select the image orientation -- ‘Landscape’ or ‘Portrait’ -- which will position the images correctly in relation to each other in the gallery.


---

## Files / Media

​​Other file types follow the same process as described above.
