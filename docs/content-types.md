# Content Types

From the top Admin menu, select Content > Add Content > [Content Type you wish to add].

![FPO Screenshot Image Alt](img/content-types/content-example.png)

Note: There are a few fields common to all content types that are explained once in the description of the Basic Page. Please review that content type first before creating any others: that is where you will find information Ordering Rows, the [Menu Setting](/en/latest/content-types/#menu-settings) and [URL Alias](/en/latest/content-types/#url-alias) fields, as well as [Revisions](/en/latest/content-types/#revisions).

---

## Alert

![Alert example](img/content-types/alert/alert-example.png)

There are two levels of Alerts: High and Low. High-level alerts are for emergencies, take over the entire page, always have a black background with white text, and are not dismissible by site visitors. Low-level alerts appear at the top of all pages are can be different colors to indicate severity (similar to [callouts](/en/latest/wysiwyg/#callout-button): <span style="color: #154734">Primary</span>, <span style="color: #3A913F">Secondary</span>, <span style="color: #789F90">Success</span>, <span style="color: #F8E08E">Warning</span>, and <span style="color: #FF6A39">Alert</span>.

![Alert editing example](img/content-types/alert/alerts-edit.png)

1. Enter Alert Title. (Note: a red asterisk next to a field name indicates it is a required field.)
2. Enter Alert text in the Body field.
3. Select Alert Type from the dropdown.
4. Select Alert Level from the dropdown (applies only to Low-level alerts).
5. Click ‘Save’ to save your work.

---

## Award

![Award example](img/content-types/award/award-example.png)

The Award content type is displayed as a detail page (shown here), and is also used to populate the [Awards and Rankings](https://www.calpoly.edu/awards) page in the site and the [Content List Award](#) component that can be used on landing pages. In the last two instances, the title and the date are displayed in a card (like the three at the bottom of the screenshot above) which links to the detail page.

![Award editing example](img/content-types/award/awards-edit-1.png)

Award Types are [Taxonomy](/en/latest/misc/) terms.
<ol>
  <li>Select Award Type from dropdown.
</li>
  <li>Enter Award Date.</li>
  <li>Enter Award Title. Text in the Title field can, in some Content Types, be changed from green to gold by adding <code>&lt;strong&gt;</code> tags, as shown in this example.</li>
  <li>Enter Subtitle, if any.</li>
  <li>If you would like your Award to have a hero image, either upload it by clicking ‘media add page.’ </li>
</ol>

See [Adding Media to the Site](/en/latest/adding-media/) or select it by using the type-ahead ‘Use existing media’ field.

![Award editing example](img/content-types/award/awards-edit-2.png)

<ol start="6">
  <li>Enter Body text. Note that in this example the cursor was placed before any of the text and an image was uploaded and then styled Image Right 50 Widget <a href="/en/latest/wysiwyg/#image-button"> Image Button,</a> for more information on this).</li>
  <li>Select College/Division from dropdown (can be ‘None’).</li>
  <li>Enter titles of Related Awards if you would like to have the award cards appear at the bottom of your page. These are type-ahead fields, and remember that if <code>&lt;strong&gt;</code> tags have been added to transform the text to gold you will need to enter those tags to find the content. (Note: Type-ahead fields are indicated by the faint circle on the right side of the field.)</li>
  <li>Click ‘Save’ to save your work.</li>
</ol>
\* You can change the order in which the Related Awards appear by adjusting the row weight via the dropdowns.

---

## Basic Page

![Basic Page example](img/content-types/basic-page/basic-page-example.png)

Basic pages can have hero images, as well as all of the features detailed in the [WYSIWYG Field Styles, Formats and Options](/en/latest/wysiwyg/#wysiwyg-field-styles-formats-and-options) section. Most, however, will be more simple pages with some text formatting.

Because this example does not have a hero area image, caption, or button links, many of the fields at the top of the template are not used.

![Basic Page example](img/content-types/basic-page/basic-page-edit-1.png)

1. Enter page Title.

![Basic Page example](img/content-types/basic-page/basic-page-edit-2.png)

<ol start="2">
<li>Enter your text in the Body field and use the <a href="/en/latest/wysiwyg/#wysiwyg-field-styles-formats-and-options"> WYSIWYG</a> buttons and dropdowns to style and format it.

\* Text format should be left in Basic HTML.</li></ol>

![Basic Page example](img/content-types/basic-page/basic-page-edit-3.png)

<ol start="3">
<li>Select related College/Department from dropdown (if any).</li>
<li>Attach any related documents using the type-ahead Use Existing Media field if they have already been uploaded, or upload (see <a href="/en/latest/adding-media/">Adding Media to the Site</a>) and then select them in the type-ahead field.</li>
<li>Select Sites Categorization from dropdown. This is a multisite build and the Admissions, Financial Aid, Gateway, and News content is all here. Sites Categorization is a sortable field from Content page: selecting it here in the dropdown allows site editors to see all of the pages grouped together by site, making it easier to find content. It also determines the theme that your page will use: please select the correct categorization from this dropdown.</li>
<li>Enter Cyclical Content tag in the type-ahead field, if applicable.</li>
<li>Enter Cyclical Content notes, if applicable.</li></ol>

\* In rare instances you may not want side navigation (e.g. a basic page that lives in the footer would not need sidebar navigation). Check ‘Hide Side Navigation’ (directly below the Sites Categorization dropdown) if you don’t want sidebar navigation on the page.

### Reordering Content Rows

\*\* Content can be reordered by clicking on the arrow of the Order dropdowns and changing the weight. On pages with many content rows, it is much easier to reorder the rows if the rows are first collapsed.

![Reorder Content Row screenshot](img/content-types/basic-page/basic-page-row-1.png)


(A) Click ‘Collapse all’

![Reorder Content Row screenshot](img/content-types/basic-page/basic-page-row-2.png)

(B) Select desired order from the dropdowns; save.
You can also click the blue ‘Hide row weights’ link, hover over and select the intersection arrows icons that appear on the left side of the content, and then drag and drop the rows into the desired position (not shown).

![Basic Page example](img/content-types/basic-page/basic-page-edit-4.png)

Screenshot above shows right side of Basic Page template. These fields appear on all content types but as this is the one that will be most widely-used they are being explained here.

### Menu Settings

<ol start="8">
<li>Click arrow to the left of ‘MENU SETTINGS’ to open accordion so you can add page to a menu. (Virtually all pages should be added to a menu.)</li>
<li>Check ‘Provide a menu link’ checkbox.</li>
<li>Enter the name for your link in ‘Menu link title’ field.</li>
<li>Select the page that is the parent to your new page in the ‘Parent Item’ dropdown.</li></ol>

### URL Alias

<ol start="12">
<li>Click arrow to the left of ‘URL ALIAS’ to open accordion and create URL alias.</li>
<li>Uncheck ‘Generate automatic URL alias’ checkbox.</li>
<li>Enter URL alias, beginning with a leading backslash and the name of the site (this will be /admissions, /financial-aid, or /news – there is no need for /gateway). Please check with University Marketing on naming conventions.</li>
<li>Click ‘Save’ at the bottom of the page (and to the left) to save your page.</li></ol>

\* The Revision log message field can be useful to let other site editors know what changes you have made and why.

### Revisions

![Revisions screenshot](img/content-types/basic-page/basic-page-revisions-1.png)

*When you add or edit a content type these four tabs will be at the top of the template, with Edit activated. The revisions tab allows you to review and/or revert to a previous version of an existing template.*

1. Click ‘Revisions’ tab to go to Revisions page.

![Revisions screenshot](img/content-types/basic-page/basic-page-revisions-2.png)

<ol start="2">
<li>Click on the date of the revision to view it. This will show you the rendered page, not the content type in edit mode.</li>
<li>Click the ‘Revert’ button to revert the page to an earlier version. You can then edit it if you’d like by the usual process.</li>
<li>Click the arrow to the right of ‘Revert’ to open the dropdown and select ‘Delete’ if you wish to delete a page revision.</li></ol>

---

## College

The College content type is basically a landing page, with select components that can be used: Hero (see [Landing Page](/en/latest/content-types/#landing-page) for Hero area content entry), College Intro, Major, [Two Column with Image](/en/latest/landing-page-components/#two-columns-with-image-list), [Major Finder](/en/latest/landing-page-components/#major-finder), Learn by Doing, Career Opportunities and Faculty Feature (Career and Faculty both use [Simple Content](/en/latest/landing-page-components/#simple-content)), and Giving Section ([Factoid List](/en/latest/landing-page-components/#factoid-list) is available but not currently being used). College pages should be consistently structured and formatted, although not all will need the Two Column with Image component.
How content is entered in the different components is mostly explained in the [Landing Page Components](/en/latest/landing-page-components/) section of this document. The components shown here will be just those that are unique to the Colleges content type.
These are also long scrolling pages – to long for a screenshot. To see an example, view one of the college pages listed below:

* [College of Agriculture, Food and Environmental Sciences](https://www.calpoly.edu/college-of-agriculture-food-and-environmental-sciences)
* [College of Architecture and Environmental Design](https://www.calpoly.edu/college-of-architecture-and-environmental-design)
* [College of Engineering](https://www.calpoly.edu/college-of-engineering)
* [College of Liberal Arts](https://www.calpoly.edu/college-of-liberal-arts)
* [College of Science and Mathematics](https://www.calpoly.edu/college-of-science-and-mathematics)
* [Orfalea College of Business](https://www.calpoly.edu/orfalea-college-of-business)

### College Intro

![College intro example](img/content-types/college/colleges-intro-example.png)

1. Enter text and use the WYIWYG to style as shown above.

### Major

 A group of people sitting and looking at the camera

![Majors section example](img/content-types/college/colleges-major.png)

Images for each of the majors offered by a college are displayed below the intro area. These link to the Major (previously created) pages.

![Majors section example](img/content-types/college/colleges-major-edit.png)

1. Enter name of major in type-ahead field. Add more type-ahead fields as necessary (‘add another item’ button at the bottom of list is not shown here).

\* Majors can be reordered in two ways: 1) selecting the major by clicking on the intersecting arrows icon to the left and dragging-and-dropping; 2) clicking on the ‘Show row weights’ link and [reordering the rows](/en/latest/content-types/#reordering-content-rows).

### Learn By Doing

![Learn by Doing section example](img/content-types/college/colleges-learn-by-doing-example.png)

The Learn by Doing component is made up of a WYSIWYG Intro area and three cards.

![Learn by Doing editing example](img/content-types/college/colleges-lbd-edit-1.png)

1. Enter your text in the WYSIWYG intro area, with ‘Learn by Doing’ formatted as Heading 1.

![Learn by Doing editing example](img/content-types/college/colleges-lbd-edit-2.png)

<ol start="2">
<li>Add your image to the card, either uploading it or selecting in the type-ahead field. (<a href="/en/latest/adding-media/">See Adding Media to the Site > Images</a>.)</li>
<li>Add Eyebrow text (optional; appears above the title) and Title text.</li>
<li>Enter Content text (descriptive text that appears below the Title). If your text needs no styling, use the Content field; otherwise use the Content HTML field and use the WYSIWYG editor to style your text.</li>
<li>If you want to include a link at the bottom of the card, enter the URL and link text here.
Enter content for the other two Learn by Doing (each College should have three).</li></ol>

### Career Opportunities / Faculty

![Faculty section example](img/content-types/college/colleges-faculty-example.png)

Both the Career Opportunities and Faculty section on the College pages use the [Simple Content](/en/latest/landing-page-components/#simple-content) (centered) component, which is explained under the Landing Pages Components section of this document. What is important on the College pages is to be sure to format these in the same way so that all College pages are consistent.

### Giving Section

![College Giving Section example](img/content-types/college/colleges-giving-section-example.png)

The Giving Section uses a special [Callout](/en/latest/wysiwyg/#callout-button) that includes the ‘The Power of Doing’ logo. The button link should go to the College page on the Giving site.

![College Giving Section example](img/content-types/college/colleges-giving-section-edit-1.png)

<ol start="6">
<li>Enter text in the WYSIWYG, create link to the College’s page on the Giving site, and then select ‘Button Link’ from the Styles dropdown.</li></ol>

![College Giving Section example](img/content-types/college/colleges-giving-section-edit-2.png)

<ol start="7">
<li>Enter URL and Link text for the college website.</li>
<li>Upload or select image to be used in the <a href="/en/latest/landing-page-components/#content-list-college">Content List College</a> landing page component. (See <a href="/en/latest/adding-media/">Adding Media to the Site > Images</a>.)</li>
<li>Select the college in the Colleges/ Divisions dropdown.</li>
<li>Click ‘Save’ to your work.</li>
</ol>

---

## Event

![Event example](img/content-types/event/event-example.png)

The Event content type has many options and a lot of flexibility: the above example shows an inline image rather than a hero, with three separate fields of event information in the main body content area and event details in the sidebar.

![Event editing example](img/content-types/event/events-edit-1.png)


1. If an event is related to a college or division, beginning typing the name and then select it from the list that drops down.
1. Use the type-ahead field to find and then select an Event Type for the event.
1. Enter a Title and a subtitle (optional) for the event.
1. Add a Featured Image, if any (see [Adding Media to the Site](/en/latest/adding-media/)). This will appear in the hero area and be used as the thumbnail image when the event is displayed in a list.
1. If the event does not have a Featured Image, the first letter of the title will be displayed in a colored box (see step no. 4 for [News](/en/latest/content-types/#news); Events and News both have this feature). In this example the letter used would be ‘T’ for test, however this can be overridden by entering a different letter in the Letter Override field.

![Event editing example](img/content-types/event/events-edit-2.png)

<ol start="6">
<li>Enter event Start and End dates – both are required, along with times.</li>
<li>Enter Venue Name (required).</li>
<li>Enter Venue Address: Street address, City, State, and Zip code are all required.</li>
<li>Enter Venue Map Link. This is optional but very helpful!</li></ol>

![Event editing example](img/content-types/event/events-edit-3.png)

<ol start="10">
<li>Enter Ticket Prices.</li>
<li>Enter Ticket Info.</li>
<li>Enter URL to third-party site where ticket can be purchased (optional).</li>
<li>Check appropriate boxes to indicate who the event is open to.</li></ol>

The main Event content is entered in Horizontal Tabs. This way you can break it up in whatever way makes the most sense (e.g. Event Info, Speakers, Sponsors, Additional Information, etc.).

![Event editing example](img/content-types/event/events-edit-4.png)

<ol start="14">
<li>Enter Horizontal Tab Title – this is what is displayed in all caps in the green bar.</li>
<li>Enter content, styling using the WYSIWYG editor.</li>
<li>Click the ‘Add Horizontal Tab’ button to add another content tab.</li>
<li>Check ‘Featured’ checkbox to include events in the featured area event pages filtered by category. Note that the most recent featured events appear here.</li>
<li>Click ‘Save’ to save your work.</li></ol>

---

## Event Series

![Event Series example](img/content-types/event-series/event-series-example.png)

The Event Series content type has a hero area similar to the [College Intro](/en/latest/content-types/#college) area (green card w/text floated over the hero image), followed by a basic WYWISYG Body field and then a list of events that are in that series (shown above). These are created before the Event Series and then associated with it.

![Event Series example](img/content-types/event-series/event-series-edit-1.png)

1. Enter Event Series Title.
1. Enter Start and End dates. These will be displayed written out, without the time of day, in gold text below the event title on the green card in the hero area.
1. Enter descriptive text for the series. Ideally this will not be more than one paragraph.

![Event Series example](img/content-types/event-series/event-series-edit-2.png)

<ol start="4">
<li>Enter Related Events Title – this will be the headline above the listed events.</li>
<li>Add events to the list by beginning to type their title in the type-ahead field.</li></ol>

![Event Series example](img/content-types/event-series/event-series-edit-3.png)

<ol start="6">
<li>Add hero image, either by clicking ‘media add page’ and uploading it or, if it has already been uploaded, use the type-ahead ‘Use existing image’ field to find and select it. (See <a href="/en/latest/adding-media/">Adding Media to the Site</a>.)</li>
<li>Enter Subtitle text, if any. This will appear in the green card in the hero area in white text, below the horizontal rule that is under the event series date.</li>
<li>Click ‘Save’ to save your work.</li></ol>

---

## External News Story

The External News Story content type is used for both the Cal Poly Magazine and the Cal Poly In the News features at the bottom of the News homepage.

![External News Story example](img/content-types/external-news-story/external-news-story.png)

### Cal Poly Magazine

![External News Story editing example](img/content-types/external-news-story/external-news-cal-poly-edit-a.png)

1. Check ‘is calpoly magazine’
2. Enter story title.
3. Add image. If the image has already been uploaded to the site you can select it by typing the file name in the type-ahead Use existing media field. To upload the image, use the ['media add page'](/en/latest/adding-media/) link.

\* Note: All External News Stories (Cal Poly Magazine type) must have an image. If one is unavailable the first letter of the story title will be used in a graphic unless you override it by entering a different letter in the Letter Override field.

![External News Story editing example](img/content-types/external-news-story/external-news-cal-poly-edit-b.png)

<ol start="4">
<li>Enter Publish Date.</li>
<li>Enter link to the story on the external site in the URL field.</li>
<li>Enter Article Description. This should be no more than ~25 words.</li>
<li>Click ‘Save’ to your work.</li></ol>

\* The Link Text field is not being used; the title of the article will contain the link.

\*\* This field is used for Cal Poly In the News only.

\*\*\* These fields are not being used and can be ignored.

### Cal Poly In the News

These External News Stories are created following the same process detailed above, with the following exceptions:

* Do not check ‘is calpoly magazine’ at the top, as indicated in step 1
* Enter the name of the article source (external newspaper or magazine)
* Leave the Article Description field blank (step 6 above)

---

## Form Document

![Form Document example](img/content-types/form-document/form-document-example.png)

Form documents provide a brief description of a form, with a link to either the form or to more information on the form, as well as links to related forms in the sidebar.

![Form Document example](img/content-types/form-document/form-document-edit-1.png)

1. Select Applicable Term for the form from the dropdown.
2. Enter Form Title.
3. Enter Date Updated.
4. If you are creating a new Form Document, you will not see the Summary field until you click on ‘Edit Summary.’ The summary text is what will appear on a Forms List page. If you do not use the Summary field, Drupal will take a trimmed version of what is entered in the Description field. This may be incomplete: on all form documents, be sure to use the Summary field!
5. Enter Summary text.
6. Enter text to appear on the Form Document page in the Description field.

![Form Document example](img/content-types/form-document/form-document-edit-2.png)

<ol start="7">
<li>Enter URL and Link text to online form (or other online resource; optional).</li>
<li>Link to previously uploaded PDF – see <a href="/en/latest/adding-media/">Adding Media to the Site,</a> for more information (optional).</li>
<li>You can add links to related forms to the sidebar of the form document by using the type-ahead field: begin typing the name of the form and select it when it comes up.
</li></ol>

\* Note: The Form Document template has checkboxes for College/Department and Audience: this is for filtering purposes and is not currently being used, however you must select an Audience as this is a required field.


---

## Historical Fact

![Historical Fact example](img/content-types/historical-fact/historical-fact-example.png)

Historical Facts can have hero image and, if one of at least four in the same category, can be part of a timeline that is created dynamically and added to the bottom of the page. The screenshot above shows one with the timeline but without a hero image. You can also choose not to have a timeline at the bottom of the page.

![Historical Fact editing example](img/content-types/historical-fact/historical-fact-edit-1.png)


1. Enter Hero Caption, if any.
2. Add (optional) hero image, either by clicking ‘media add page’ and uploading it or, if it has already been uploaded, use the type-ahead ‘Use existing image’ field to find and select it. (See <a href="/en/latest/adding-media/">Adding Media to the Site > Images</a>.)
3. Add URLs and Link text for up to three (optional) CTA buttons that will appear in the hero area below the Hero Caption.

![Historical Fact editing example](img/content-types/historical-fact/historical-fact-edit-2.png)

<ol start="4">
<li>Enter Eyebrow text. For Historical Facts, this is typically a date or a date range.</li>
<li>Enter Historical Fact Title.</li>
<li>Enter Subheader.</li>
<li>Add Portrait image by the same process used to add a Hero Image.</li>
<li>Click ‘Edit summary’ to open the summary field.</li></ol>

![Historical Fact editing example](img/content-types/historical-fact/historical-fact-edit-3.png)

<ol start="9">
<li>Enter Summary text. This is important! This is the brief sentence that appears below the image when it is in a timeline.</li>
<li>Enter Body text.</li>
<li>Enter Historical Date. This is used to order Historical Facts in a timeline and not displayed on the page: while you do not have to have the exact date, what you enter here will determine where the item is displayed in relation to others in the same timeline (category).</li>
<li>Select Historical Fact Category from the dropdown. ‘None’ is an option and should be selected if your Historical Fact will not be part of a timeline.</li>
<li>Click ‘Save’ to save your work.</li></ol>

### Editing the Timeline Title
Historical Facts of the same category are displayed in a timeline at the bottom of each Historical Fact of that category. (Note: you must have at least four Historical Facts per timeline). The categories are [Taxonomy terms](/en/latest/misc/#editing-adding-taxonomy-terms).

Each timeline has a title that appears above the timeline -- in this example, title for the Leadership category historical fact timeline is ‘Cal Poly Directors (1902 – Present)’.

![Historical Fact Timeline Title editing example](img/content-types/historical-fact/historical-fact_timeline-title-1.png)

1. From the top Admin menu, click on ‘Structure’ then go to ‘Taxonomy’ then click on ‘Historical Category’ to get to the list of categories.

![Historical Fact Timeline Title editing example](img/content-types/historical-fact/historical-fact_timeline-title-2.png)

<ol start="2">
<li>Click the ‘Edit’ button of the Historical Category title you want to change.</li></ol>

![Historical Fact Timeline Title editing example](img/content-types/historical-fact/historical-fact_timeline-title-3.png)

<ol start="3">
<li>Edit the title of the Historical Category in the WYSIWYG.</li>
<li>Click ‘Save’ to save your work.</li></ol>

---

## Landing Page

![Landing Page example](img/content-types/landing-page/landing-page.png)

*A Landing Page is made up of a hero area, optional subnav, and a series of [Landing Page Components](/en/latest/landing-page-components/).*
*How to populate the hero area is explained below; instruction on how to create the different Landing Page Components can be found later in this document.*

![Example of editing the Landing Page hero](img/content-types/landing-page/landing-page-edit-hero.png)

1. Enter Page Title. This is what will appear in the Admin view list of all of the content.
2. Click ‘Hide Subnav’ to remove subnavigation that would otherwise appear below the hero image. (Note: This option is rarely used.)
3. Enter Hero Caption. This is the text that appears over the image.
4. Add hero image, either by clicking ‘media add page’ and uploading it or, if it has already been uploaded, use the type-ahead ‘Use existing image’ field to find and select it. (See <a href="/en/latest/adding-media/">Adding Media to the Site > Images</a>.)
5. While they are not shown in this example, you can add up to three button links below the hero area (see <a href="https://www.calpoly.edu/">Calpoly.edu home page</a> for an example of them in use). Enter URL and Link text here if you wish to add the them to your hero area.

![FPO Screenshot Image Alt](img/content-types/landing-page/landing-page-edit-2.png)

<ol start="6">
<li>Add your landing page components by selecting them from the dropdown. You can add as many as you’d like, and reorder them once they have been created by changing the row weight in the dropdown to the right or dragging and dropping them into the desired order. (See <a href="/en/latest/content-types/#reordering-content-rows">Reordering Content Rows</a>.)</li>
<li>Click ‘Save’ to save your work.</li>
</ol>

---

## Major

The Major content type (like the College content type) is basically a landing page, with select components that can be used: Hero (see [Landing Page](/en/latest/content-types/#landing-page)for Hero area content entry), Major Intro, Degree Requirements (uses [Horizontal Tab](/en/latest/landing-page-components/#horizontal-tabs) component), About the Program ([Simple Content](/en/latest/landing-page-components/#simple-content)), Learn by Doing , Our Graduates ([Simple Content](/en/latest/landing-page-components/#simple-content)), and Visit Department Website. ([Factoid List](/en/latest/landing-page-components/#factoid-list) is available but not currently being used). Major pages should be consistently structured and formatted.

How content is entered in the different components is mostly explained in the [Landing Page Components](/en/latest/landing-page-components/#landing-page-components) section of this document. The components shown here will be just those that are unique to the Major content type.
These are also long scrolling pages – too long for a screenshot. To see an example, view one of the major pages listed below:

* [Architecture](https://www.calpoly.edu/major/architecture)
* [Wine and Viticulture](https://www.calpoly.edu/major/wine-and-viticulture)
* [Computer Engineering](https://www.calpoly.edu/major/computer-engineering)
* [Comparative Ethnic Studies](https://www.calpoly.edu/major/comparative-ethnic-studies)
* [Marine Sciences](https://www.calpoly.edu/major/marine-sciences)
* [Business Administration](https://www.calpoly.edu/major/business-administration)

### Major Intro

![Major Intro example](img/content-types/major/major-intro.png)

1. Enter text and use the WYIWYG to style as shown above.

### Degree Requirements

This area of the page uses the [Horizontal Tabs](/en/latest/landing-page-components/#horizontal-tabs) component, explained later in this document. Different majors will have a different number of tabs: the most common are Degree Requirements, Labs, and Related Minors.

### Learn by Doing

Content for the Learn by Doing section on Major pages is entered in the same way it is for the [Colleges Learn by Doing](/en/latest/content-types/#learn-by-doing) section. The only difference is that on Major pages, only one Learn by Doing examples is used.

### Visit Department Website

![Department link example](img/content-types/major/major-visit-website.png)

1. Enter URL and link text.

---

## Major Transfer Information

![Major Transfer Information example](img/content-types/major-transfer-information/major-transfer-example.png)

As the copy between the page title and Course Listings is the same on all Major Transfer Information pages it is contained in a [Custom Block](/en/latest/misc/#editing-custom-blocks) and edited elsewhere. In this content type you will be entering the Eyebrow, Title, and Course Listings content.

![Editing Major Transfer info example](img/content-types/major-transfer-information/major-transfer-info.png)

1. Enter Eyebrow text as you see it here: TRANSFER COURSE CRITERIA FOR

2. Enter name of Major in Title field.

* Both the Top Content adn the Title field below it should already be populated with 'Major transfer top content'

![Example of editing course listings](img/content-types/major-transfer-information/major-transfer-info-course-listings.png)

<ol start="3">
<li>Enter Course Listings copy in the Body field, using the WYSIWYG to style your text. Note that content for each terms is entered in a different accordion.</li></ol>

![Editing applicable terms and divisions](img/content-types/major-transfer-information/major-transfer-info-terms-college.png)

<ol start="4">
<li>Select Applicable Term from dropdown.</li>
<li>Select College from dropdown.</li>
<li>Select Sites Categorization. The Major Transfer Information pages live in the Admissions site, so that is selected here. </li>
<li>Click ‘Save’ to save your work.</li></ol>
---

## News

![News story example](img/content-types/news/news-example-1.png)

This screenshot above shows a news story detail page with a hero image, lead paragraph, embedded video, and an image gallery, with text broken up with subheads. These are all optional features. In the Body field you can also float images, include tables and lists, add blockquotes, etc., however many news stories will be much simpler.

![FPO Screenshot Image Alt](img/content-types/news/news-block-a.png)

This screenshot, above, shows what a news story will look like when displayed on the home page, on a news list page filtered by category, or in a [Content List News](/en/latest/landing-page-components/#content-list-news) component.

![News editing example](img/content-types/news/news-edit-a.png)

1. Select News Category from the dropdown.
2. Check ‘Featured’ if you want the news story to be included in the featured area of the News home or filtered-by-category news pages (note that the most recent featured news stories appear here).
3. Enter the news story Title.
4. If your story has a hero image, add it here by either clicking on ‘media add page’ and uploading it or, if it has already been uploaded, use the type-ahead ‘Use existing image’ field to find and select it. (See [Adding Media to the Site](/en/latest/adding-media/).)

![News editing example](img/content-types/news/news-edit-b.png)

<ol start="5">
<li>The Letter Override field is to be used if there is no Featured or Listing Image (for Listing Image, see no. 14 below) uploaded. News stories appear on the home and category pages with the article title, along with a rectangular image. As a featured image was uploaded in this example, this is what will be displayed with the title on these pages. If no featured photo is uploaded and nothing is entered in the ‘Letter Override’ field, Drupal will display the first letter of the article in a colored block. In this instance, the ‘E’ from ‘Example’ would have been displayed if an ‘A’ had not been entered (see above right).</li>
<li>Enter story Author, if any.</li>
<li>Enter Publish Date. This is the date of the story that is displayed on the page.</li></ol>

![News editing example](img/content-types/news/news-edit-c.png)


<ol start="8">
<li>For the body of the news story, select either ‘Add News Content row’ or ‘Add News Image Gallery’ to add the field to the page. The example has a News Content row at the top, as will most news stories.</li></ol>

![News editing example](img/content-types/news/news-edit-d.png)


<ol start="9">
<li>Enter your body content using the WYSIWYG editor.</li></ol>

\* Content on News and Landing pages is entered in rows. These rows of content can be reordered using the numbers to the right of the field – see [Reordering Content Rows](/en/latest/content-types/#reordering-content-rows) for information on how to do this.

![News editing example](img/content-types/news/news-edit-e.png)

<ol start="10">
<li>Add additional Content Rows as needed. Note that images in a News Image Gallery row are first uploaded to the site and then selected in the type-ahead fields. This can be done from this page by clicking on the ‘media add page’ and following the steps detailed in the <a href="/en/latest/adding-media/">Adding Media to the Site</a> section of this document.</li></ol>

![News editing example](img/content-types/news/news-edit-f.png)

<ol start="11">
<li>Enter News Topics, separated by commas. These appear as tags at the bottom of the article.</li>
<li>Select College or Department the news story is associated with, it any.</li>
<li>Select Audience the news story is for.</li></ol>

![News editing example](img/content-types/news/news-edit-g.png)

<ol start="14">
<li>Upload Listing Image (if desired). There may be cases in which you want to use a photo for the Featured (hero) image that won’t work well for the list display of the story on the home or filtered category page (e.g. it could be too tall or wide). This field allows you to have different images for the story: one for the news detail page, and another for the list display.</li>
<li>Click ‘Save’ to save your work.</li></ol>

---

## Newsletter

![Newsletter example](img/content-types/newsletter/newsletter-example.png)

Above is a very truncated screenshot of a newsletter: these tend to be very long (as indicated by the navigation on the right) but are easy to create as they are comprised of a hero area followed by series of WYSIWYG fields, with an optional block at the bottom.

![Newsletter editing example](img/content-types/newsletter/newsletters-edit-1.png)

1. Add hero image by either upload it by clicking ‘media add page’ and following the directions from step two ([see Adding Media to the Site](/en/latest/adding-media/)) or select it by using the type-ahead Use existing media field.
\* The Hero Caption and (optional) Button Link fields are not used in this content type.

![Newsletter editing example](img/content-types/newsletter/newsletters-edit-2.png)

<ol start="2">
<li>Enter newsletter Title.</li>
<li>Enter newsletter issue Date.</li>
<li>Add PDF version of newsletter by clicking ‘media add page’ link and uploading your file. <a href="/en/latest/adding-media">See Adding Media to the Site</a>.</li>
<li>Add (optional) intro text.</li>
 * All of the above appears in the green card that overlays the hero image.
</ol>
![Newsletter editing example](img/content-types/newsletter/newsletters-edit-3.png)

<ol start="6">
<li>Enter newsletter section Title.</li>
<li>Enter content for the section in the WYSIWYG, following established styles as set by University Marketing.</li>
<li>Click ‘Add Horizontal Tab’ to add another section to the newsletter.</li></ol>

![Newsletter editing example](img/content-types/newsletter/newsletters-edit-5.png)


<ol start="9">
<li>If you would like to remove the Newsletter Block from the bottom of your newsletter, select ‘none’ from the Block Content dropdown. It is added by default, along with the display title. Instructions on how to edit it can be found under <a href="/en/latest/misc/#newsletter-bottom-block-content">Editing Custom Blocks > Newsletter Bottom Block Content</a>.</li>
<li>Click ‘Save’ to save your work.</li></ol>

---

## Webform

![Webform example](img/content-types/webform/webform-example.png)

### Selecting and Defining the Elements in a Webform

1. From the top Admin menu, click on ‘Structure’ and select Webforms.
2. Click ‘Add webform’.

![Webform example](img/content-types/webform/webform-edit-1.png)

<ol start="3">
<li>Enter webform title.</li>
<li>Enter Administrative description. This will appear in the list of webforms. It is not visible to site visitors.</li>
<li>Select webform Category from the dropdown list.</li>
<li>Click 'Save.'</li></ol>

![Webform example](img/content-types/webform/webform-edit-2.png)

<ol start="7">
<li>Click ‘Add element’ and then select and define (choose the settings, conditions, etc.) the elements of the form.
Note: The Webform module has many different features and options. For more information on how it works, see <a href="https://www.drupal.org/docs/8/modules/webform/webform-introduction">Webform</a>.</li></ol>

![Webform example](img/content-types/webform/webform-edit-3.png)

<ol start="8">
<li>Once you have added and defined all of your elements, click ‘Save elements.' Now you need to create a page for the form.</li>
</ol>

### Creating a Page for a Webform

9) From the top Admin menu: Content > Add Content > Webform.

![Webform example](img/content-types/webform/webform-edit-4.png)

<ol start="10">
<li>Enter Title: this will be the title of the web page.</li>
<li>Enter any test you want to appear above the webform.</li>
<li>Select the webform from the dropdown list to add it to the page.</li></ol>

![Webform example](img/content-types/webform/webform-edit-5.png)

<ol start="13">
<li>Select Submission status.</li>
<li>Click ‘Save’ to save your work.</li></ol>
