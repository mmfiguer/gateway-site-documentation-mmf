# The Pattern Library

![Screenshot of the pattern library home page](img/pattern-library/1.png)

​​The Pattern Library is a visual design reference for the site. It includes information on elements, such as colors and typography, as well as components (design blocks within different templates). As of April 2020, it is [hosted by Rolling Orange](http://calpoly.rollingorange.com/phase-one/deliverables/design-prototype/final/) but the URL will change when it is moved to a Cal Poly server.

​​If a component uses an image, you will find dimensions for the image in a green box below the component in the Pattern Library.

![Screenshot of pattern library image dimensions](img/pattern-library-image-dimension-guides.png)
