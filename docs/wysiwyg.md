# WYSIWYG Field Styles, Formats and Options

![WYSIWYG Editor screenshot](img/wysiwyg/wysiwyg-button-bar.png)

Many content areas in the new website contain WYSIWYG fields, in which text and content is styled in much the same manner as it is in Word. Here, we explain the options which may be a bit different from what you are used to or expect.

---

## Horizontal Rule Button

![Horizontal rule screenshot](img/wysiwyg/hr/horizontal-rule.png)

To insert a Horizontal Rule, click on the button indicated with an (A) in the screenshot at the top of this section.

---

## Add/Remove Link Button

![Edit link screenshot](img/wysiwyg/add-remove-link/add-remove-link-edit-1.png)

1. Highlight the text you want to link (don't include punctuation marks) and then click the indicated icon.  

![Edit link screenshot](img/wysiwyg/add-remove-link/add-remove-link-edit-2.png)

<ol start="2">
<li>Select ‘Link List’ from the Styles dropdown.</li>
<li>If you are linking to a page within the site, begin typing the name of the page and then select it from the dropdown. If you are linking to an external site, enter the URL (including http:// or https://).</li>
</ol>

To remove the link, highlight it then click the grayed-out icon to the right of the one indicated above.

### Button Links

![Button Link Screenshot](img/wysiwyg/button-links/button-links-example.png)

Above are all of the button links styles available. They use the same name in the Styles dropdown list.

![Button Link Screenshot](img/wysiwyg/button-links/button-link-edit.png)

1. Highlight the text you want to make into a button.
2. Click ‘Add Link’ icon and enter the URL (see above if you need more information).
3. While the link text still selected (and now underlined to show that the link has been added), choose the button style you want from the Styles dropdown (full name of the style will appear in a tag upon hover).

---

## List Buttons

​​Basic lists (bullets or numbered) are created like they are in Word: create a list, highlight it, and then click the icon for either unordered (bullets) or ordered (numbered). The Cal Poly site offers advanced list display, as well.

### Link Lists

![LinkList Screenshot](img/wysiwyg/list-button/link-list-example.png)

​​A link list denotes different file types by displaying an icon before the linked file. These icons are associated in the system and will appear if you choose this list option.

![LinkList Editing Screenshot](img/wysiwyg/list-button/link-list-edit-1.png)

1. Enter your list of files and highlight them.
2. Click the ‘Unordered List’ icon.
3. Create your links. (See [Add/Remove Link](/en/latest/wysiwyg/#addremove-link-button). You will need to have uploaded your files already in order to select them; see [Adding Media to the Site > Files/Media](/en/latest/adding-media/).) Then click on the ‘Ordered List’ icon.

![LinkList Editing Screenshot](img/wysiwyg/list-button/link-list-edit-2.png)
<ol start="4">
<li>Select ‘Link List’ from the Styles dropdown.
Note: each list item must be linked. If your list includes non-linked words, it will not work. </li>
</ol>

### Step-by-Step List

![Step-by-Step List Screenshot](img/wysiwyg/list-button/stepped-list-example.png)

The Step-by-Step list is created in the same way as the Link List, with the following differences:

1. Chose Ordered List (with the numbers, below) instead of Unordered list (with bullets)

![Step-by-Step List Editing Screenshot](img/wysiwyg/list-button/stepped-list-edit-1.png)

<ol start="2">
<li>Select ‘Stepped list’ from the Styles dropdown. ​​Note: each list item must be linked. If your list includes non-linked words, it will not work</li>
</ol>

---

## Blockquote Button

![Blockquote example](img/wysiwyg/blockquote/block-quote-simple-edit-1.png)

​​Blockquotes help to highlight content on a page, especially in news stories. There are two ways to create a blockquote — simple and styled. The simple way uses the WYSWYG editor. The styled version requires you to switch to source mode in the WYSIWYG editor and add a few HTML tags.

### Simple Blockquote

![Blockquote example](img/wysiwyg/blockquote/block-quote-simple-edit-1.png)

1. Enter your text.

![Blockquote example](img/wysiwyg/blockquote/block-quote-simple-edit-2.png)

<ol start="2">
<li>Highlight the text you want to be styled.</li>
<li>Click ‘Blockquote’ button. This will add the large open quote (see next screenshot). ​​This is all you need to do to create a simple blockquote. To create a styled blockquote, continue to the next example.</li>
</ol>

### Styled Blockquote

![Blockquote example](img/wysiwyg/blockquote/block-quote-styled-edit-1.png)

1. Click ‘Source’ button to edit HTML markup / add tags to style the text.

![Blockquote example](img/wysiwyg/blockquote/blockquote-styled-edit-2.png)

<ol start="2">
<li>Add <code>&lt;cite&gt;</code> tag before the name of the person quoted and <code>&lt;/cite&gt;</code> tag before the <code>&lt;/p&gt;</code> tag.</li>
<li>Add <code>&lt;em&gt;</code> tag before first word in the second line (this may not always be degree and year) and <code>&lt;/em&gt;</code> tag before the <code>&lt;/cite&gt;</code> tag.</li>
<li>Click ‘Source’ button again to get back into WYSIWYG mode.</li>
</ol>

---

## Image Button

![Image button example](img/wysiwyg/image-button/example-image-button.png)

Images added using the WYSIWYG can be styled and sized: the image above was added and then placed on the right side of the page, set to take up 50% of the content area.

![Image button example](img/wysiwyg/image-button/image-button-edit-1.png)

1. Place your cursor where you want your image to be on the page (in this example, at the top of the page*) and click the ‘Image’ button.

![Image button example](img/wysiwyg/image-button/image-button-edit-2.png)

<ol start="2">
<li>Click ‘Choose file’ button, then on your computer find and then select the image you want to upload to the site. (Note: a red asterisk next to a field name indicates it is a required field.)</li>
<li>Enter Alternative text.</li>
<li>Click ‘Save’.</li>
</ol>

\* If your image is already at the size you want it you can set the alignment here. If you set alignment this way you cannot use the Style function explained in numbers 5 and 6 below.

\* Check ‘Caption’ if you want to add a caption below your image (not shown in this example). The caption, which would be added in the next screen, appears below the image in smaller text.


![Image button example](img/wysiwyg/image-button/image-button-edit-4.png)
<ol start="5">
<li>Click on your image to select it.</li>
<li>From the Styles dropdown, select the orientation and size for your image. (Note:  when you hover over the selection a tag telling you the size will appear – this is Image Right 50 Widget).</li>
</ol>

![Image button example](img/wysiwyg/image-button/image-button-edit-3.png)

The image will look a little different in the WYSIWYG now – not exactly as it will on the published page, but you can see that it is smaller and the position has shifted. When you save the page and view it, it will appear as it does in the first Image Button screenshot.

---

## Embed Media Button

This button is not being used: use the [Embed YouTube Video Button](/en/latest/wysiwyg/#embed-youtube-video-button) instead.

---

## Accordion (Collapsible Item) Button

![Accordion example](img/wysiwyg/accordion/accordions-example.png)

​​Accordions are helpful when you have a lot of content on one page, but not every user needs to read all of the content. An example could be a Frequently Asked Questions page. Each question can be a visible topic, with its answer hidden until a user expands the specific accordion to read more. ​​This approach allows users to scan a page to quickly find the specific content they need.


![Accordion editing example](img/wysiwyg/accordion/accordions-edit-1.png)

1. With your cursor in the WYSIWYG field where you want to place the accordion, click the ‘Accordion’ button.
2. Enter text that is always visible in the top field, typing over ‘Header goes here’.
3. Enter text that will be hidden in the field below, typing over ‘Content goes here’. Note that this field can contain formatted text, images, etc.
4. To add another accordion or more content below the one you have created, hover over the bottom right corner of the accordion and then click the red ‘return arrow’ button that appears.

---

## Callout Button

![Callout example](img/wysiwyg/callout/callouts-example.png)

Callouts are used to direct your audience’s attention to important information, with different styles for different uses. The process for creating them is very similar to the one used to create accordions.

![Callout editing example](img/wysiwyg/callout/callouts-edit-1.png)

1. With your cursor in the WYSIWYG field where you want to place the callout content, click the ‘Callout’ button.
2. Select the type of Callout you want from the dropdown.

![Callout editing example](img/wysiwyg/callout/callouts-edit-2.png)

<ol start="3">
<li>Enter Callout Title, typing over text displayed in the WYSIWYG.</li>
<li>Enter Callout Content, typing over text displayed in the WYSIWYG. Note that this field can contain formatted text, images, etc.</li>
</ol>

---

## Embed YouTube Video Button

 ![YouTube embedding example](img/wysiwyg/embed-youtube/youtube-embed-example.png)

Videos hosted on YouTube may be added to pages on the site.

![YouTube embedding example](img/wysiwyg/embed-youtube/youtube-embed-edit-1.png)

1. With your cursor in the WYSIWYG field where you want to place the video, click the ‘Embed YouTube Video’ button. This will bring up a modal window in which you enter the URL for the YouTube video.
2. Before entering anything in the modal window, find the video you want to add to your page on YouTube.

![YouTube embedding example](img/wysiwyg/embed-youtube/youtube-embed-edit-2.png)

<ol start="3">
<li>Click on ‘Share’ button below the YouTube Video.</li>
<li>Copy the video URL by clicking on the "copy" option.</li>
</ol>

![YouTube embedding example](img/wysiwyg/embed-youtube/youtube-embed-edit-3.png)

<ol start="5">
<li>Paste the URL in the Paste YouTube Video URL field.</li>
<li>Enter ‘1600’ into Width field and ‘900’ into Height field.</li>
<li>Check ‘Make Responsive’ box.</li>
<li>Click ‘Okay’.</li>
</ol>

---

## Table Button

![Table screenshot](img/wysiwyg/table/tables-example.png)

​​There are several types of tables available in the templates. These instructions are fore the simple table. Other table styles are available via  ‘Source’ and are briefly described in Step 5.

![Table screenshot](img/wysiwyg/table/tables-edit-1.png)

1. With your cursor in the WYSIWYG field where you want to place your table, click the ‘Table’ button.
2. Enter the number of rows and columns you would like in your table.
3. If you would like a header, select it from the dropdown (options: None, First Row, First Column, Both).
4. Enter (optional) Caption, which will appear above the table.
5. Enter (optional) Summary. This text will not display, but is a place where you can annotate the HTML – this will be visible when you toggle to ‘Source’.
The basic table style is striped: you should in most cases leave the other fields -- Border Size, Alignment, Width, Height, Cell spacing, Cell padding -- alone.

![Table screenshot](img/wysiwyg/table/tables-edit-2.png)

<ol start="6">
<li>Click into the cell you want to add text to and enter your copy.</li>
</ol>

---

## Styles Dropdown

![Styles screenshot](img/wysiwyg/styles-dropdown/styles-dropdown-example.png)

As demonstrated in other areas of this document ([Step-by-Step List](/en/latest/wysiwyg/#step-by-step-list), [Button Links](/en/latest/wysiwyg/#button-links), [Image Button](/en/latest/wysiwyg/#image-button), etc.), the Styles dropdown is specific to the content selected. The above example shows text styles examples: you can see other options in the online [Pattern Library](/en/latest/pattern-library/). Select the content you wish to style to view the different styling options available from Styles dropdown.
Note: To remove an applied style, select the styled content and then go back to the Styles dropdown and select the applied styled. (e.g. You want to remove Lead styling: select the text, click the Styles dropdown and select ‘Lead’ – the text will revert to normal).

---

## Format Dropdown

The Format dropdown applies classes to tags (which styles them).

![Format dropdown screenshot](img/wysiwyg/format-dropdown/format-dropdown.png)

1. Select the text you want to format.
2. Select the format from the Format dropdown.
Note: To remove a format you have applied, select the formatted content and then use the Format dropdown to select ‘Normal’.

---

## Source

​​Click the  ‘Source’ button to toggle from the WYSIWYG view to the HTML mark up view. Some advanced styles, such as [Styled Blockquote](/en/latest/wysiwyg/#styled-blockquote), require changes in the HTML mark up view. Only use the HTML mark up view if you understand HTML. Making a mistake in the HTML mark up view can cause display problems for your entire web page display.

Click ‘Source’ button to toggle from the WYSIWYG view to see the HTML mark up. There are rare instances in which you will have to add tags to content in the Source view to format or style it: see [Styled Blockquote](/en/latest/wysiwyg/#styled-blockquote) for an example.

---

## Special Characters Button

​​You can use the ‘Special Character’ button to insert special punctuation or characters such as curly quotes, em-dashes, or letters from other alphabets.

![Special characters screenshot](img/wysiwyg/special-chars/special-character-example.png)

1. With the cursor in the WYSIWYG field, click ‘Special Character’ button.
2. Click on the special character you want to insert.
